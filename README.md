This repository contains examples of code I've written in various courses and in my free time. 

Note: Some of the examples were completed as assignments for courses, thus snippets of the code, such as test cases and function names, may have been provided by the instructor.